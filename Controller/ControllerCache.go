package Controller

import (
	"api-training-ley/Constant"
	"api-training-ley/Controller/Dto"
	"api-training-ley/Library"
	"api-training-ley/Repository"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"time"
)

func SetRedis(w http.ResponseWriter, r *http.Request) {
	var dataUser Dto.DataUser
	err := json.NewDecoder(r.Body).Decode(&dataUser)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	// syntax redis
	// - SET
	// - GET
	// - HMSET
	// - HMGET
	// - DEL
	// - HDEL
	//
	keys := fmt.Sprintf("company-id::%d::data-perusahaan", 1)
	repository := Repository.AllRepository.Public
	err = repository.SetCache(context.Background(), keys, dataUser, 0)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	Library.HttpResponseSuccess(w, r, http.StatusOK)
}

func GetRedis(w http.ResponseWriter, r *http.Request) {
	var dataUser Dto.DataUser
	repository := Repository.AllRepository.Public
	keys := fmt.Sprintf("company-id::%d::data-perusahaan", 1)
	err := repository.GetCache(context.Background(), keys, &dataUser)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	Library.HttpResponseSuccess(w, r, dataUser)
}

func SetHMRedis(w http.ResponseWriter, r *http.Request) {
	//var dataRequest map[string]interface{}
	//err := json.NewDecoder(r.Body).Decode(&dataRequest)
	//if err != nil {
	//	Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
	//	return
	//}
	//keys := "list-data-request-v1"
	//repository := Repository.AllRepository.Public
	//err = repository.SetHMCache(context.Background(), keys, dataRequest, time.Minute*2)
	//if err != nil {
	//	Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
	//	return
	//}
	//Library.HttpResponseSuccess(w, r, http.StatusOK)

	var reqDataRestaurant Dto.ReqFoods
	err := json.NewDecoder(r.Body).Decode(&reqDataRestaurant)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	restaurantName := r.URL.Query().Get("restaurantName")
	keys := fmt.Sprintf(Constant.FormatKeysRestaurant, restaurantName)
	mapRestaurantFoods := make(map[string]interface{})
	for _, val := range reqDataRestaurant.Foods {
		mapRestaurantFoods[val.Name] = val.Ingredients
	}
	repository := Repository.AllRepository.Public
	err = repository.SetHMCache(context.Background(), keys, mapRestaurantFoods, time.Minute*10)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	Library.HttpResponseSuccess(w, r, http.StatusOK)
}

func GetHMRedis(w http.ResponseWriter, r *http.Request) {
	//field := chi.URLParam(r, "field")
	//var resultRedis interface{}
	//keys := "list-data-request-v1"
	//repository := Repository.AllRepository.Public
	//err := repository.GetHMCache(context.Background(), keys, field, &resultRedis)
	//if err != nil {
	//	Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
	//	return
	//}
	//Library.HttpResponseSuccess(w, r, resultRedis)

	restaurantName := r.URL.Query().Get("restaurantName")
	keys := fmt.Sprintf(Constant.FormatKeysRestaurant, restaurantName)
	field := chi.URLParam(r, "field")
	var foodsData Dto.Food
	foodsData.Name = field
	repository := Repository.AllRepository.Public
	err := repository.GetHMCache(context.Background(), keys, field, &foodsData.Ingredients)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	Library.HttpResponseSuccess(w, r, foodsData)
}

func HGetAllRedis(w http.ResponseWriter, r *http.Request) {
	//var resultRedis interface{}
	//keys := "list-data-request-v1"
	//repository := Repository.AllRepository.Public
	//err := repository.HGetAllCache(context.Background(), keys, &resultRedis)
	//if err != nil {
	//	Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
	//	return
	//}
	//Library.HttpResponseSuccess(w, r, resultRedis)

	restaurantName := r.URL.Query().Get("restaurantName")
	keys := fmt.Sprintf(Constant.FormatKeysRestaurant, restaurantName)
	repository := Repository.AllRepository.Public
	dataRedis, err := repository.HGetAllCache(context.Background(), keys)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	var foodsData []Dto.Food
	for keys, values := range dataRedis {
		var foods Dto.Food
		foods.Name = keys
		json.Unmarshal([]byte(values), &foods.Ingredients)
		foodsData = append(foodsData, foods)
	}
	Library.HttpResponseSuccess(w, r, foodsData)
}

func HDelRedis(w http.ResponseWriter, r *http.Request) {
	//
	//field := chi.URLParam(r, "field")
	//repository := Repository.AllRepository.Public
	//keys := "list-data-request-v1"
	//err := repository.HDelCache(context.Background(), keys, field, "age", "ktp", "sdadasd", "asdasdasdads")
	//if err != nil {
	//	Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
	//	return
	//}
	//Library.HttpResponseSuccess(w, r, http.StatusOK)
	field := chi.URLParam(r, "field")
	restaurantName := r.URL.Query().Get("restaurantName")
	keys := fmt.Sprintf(Constant.FormatKeysRestaurant, restaurantName)
	repository := Repository.AllRepository.Public
	err := repository.HDelCache(context.Background(),keys,field)
	if err != nil {
		Library.HttpResponseError(w,r,err.Error(),http.StatusInternalServerError)
		return
	}
	Library.HttpResponseSuccess(w,r,http.StatusOK)
}

func DelRedis(w http.ResponseWriter, r *http.Request) {
	keys := "list-data-request-v1"
	repository := Repository.AllRepository.Public
	err := repository.DelCache(context.Background(), keys)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	Library.HttpResponseSuccess(w, r, http.StatusOK)
}
