package Controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"api-training-ley/Controller/Dto"
	"api-training-ley/Library"
	"api-training-ley/Repository"
)

//Master sembako
//Transaksi sembako

func ControllerPing(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
}

func CreateSembako(w http.ResponseWriter, r *http.Request) {
	//butuh data company
	var requestBody Dto.ReqCreateSembako
	err := json.NewDecoder(r.Body).Decode(&requestBody)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}
	repository := Repository.AllRepository
	id, err := repository.Sembako.CreateSembako(requestBody)
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	//butuh data sembako
	respSuccess := fmt.Sprintf("Success create sembako with id %d", id)
	Library.HttpResponseSuccess(w, r, respSuccess)
}

func EditSembako(w http.ResponseWriter, r *http.Request) {
	//Helper.HttpResponseSuccess(w, r, "Success Edit Sembako")
	Library.HttpResponseError(w, r, "Data not found", http.StatusBadRequest)
}

func DeleteSembako(w http.ResponseWriter, r *http.Request) {
	Library.HttpResponseSuccess(w, r, "Success Delete Sembako")
}

func ViewAllSembako(w http.ResponseWriter, r *http.Request) {
	repository := Repository.AllRepository.Sembako
	resp, err := repository.ViewAllSembako()
	if err != nil {
		Library.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	Library.HttpResponseSuccess(w, r, resp)
}
