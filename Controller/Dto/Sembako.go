package Dto

type ReqCreateSembako struct {
	Name     string  `json:"name"`
	Qty      int     `json:"qty"`
	Price    float64 `json:"price"`
	Category int     `json:"category"`
}

type ReqEditSembako struct {
	Id       int     `json:"id"`
	Name     string  `json:"name"`
	Qty      int     `json:"qty"`
	Price    float64 `json:"price"`
	Category int     `json:"category"`
}

type ResponseSembako struct {
	Id        int     `json:"id"`
	Category  int     `json:"category"`
	Name      string  `json:"name"`
	Qty       int     `json:"qty"`
	Price     float64 `json:"price"`
	CreatedAt string  `json:"created_at"`
}
