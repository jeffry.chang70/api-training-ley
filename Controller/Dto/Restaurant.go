package Dto

type ReqFoods struct {
	Foods []Food `json:"foods"`
}

type Food struct {
	Name        string       `json:"name"`
	Ingredients []Ingredient `json:"ingredients"`
}

type Ingredient struct {
	Name string `json:"name"`
	Qty  int    `json:"qty"`
}
