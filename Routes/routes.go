package Routes

import (
	"api-training-ley/Controller"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"net/http"
)

func HandleRequests() {
	r := chi.NewRouter()
	r.Group(func(r chi.Router) {
		r.Use(middleware.Logger)
		r.Route("/sembako", func(r chi.Router) {
			r.Post("/", Controller.CreateSembako)
			r.Get("/", Controller.ViewAllSembako)
			r.Put("/", Controller.EditSembako)
			r.Delete("/", Controller.DeleteSembako)
		})
		r.Route("/redis", func(r chi.Router) {
			r.Post("/", Controller.SetRedis)
			r.Get("/", Controller.GetRedis)
			r.Post("/map", Controller.SetHMRedis)
			r.Get("/map/{field}", Controller.GetHMRedis)
			r.Get("/all", Controller.HGetAllRedis)
			r.Delete("/{field}", Controller.HDelRedis)
			r.Delete("/keys", Controller.DelRedis)
		})
	})
	http.ListenAndServe(":3000", r)
}
