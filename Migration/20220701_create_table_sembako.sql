CREATE TABLE public.sembako (
    sembako_id serial PRIMARY KEY,
    sembako_name VARCHAR(100),
    sembako_category int,
    sembako_qty int,
    sembako_price numeric,
    created_at timestamp without time zone default now(),
    updated_at timestamp without time zone default now()
)

CREATE TABLE public.trx_sembako(
   trx_id serial PRIMARY KEY,
   sembako_id int,
   qty int,
   total_price numeric,
   created_at timestamp without time zone default now()
)