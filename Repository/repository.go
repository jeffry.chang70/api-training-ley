package Repository

import (
	"api-training-ley/Repository/company"
	"api-training-ley/Repository/public"
	"api-training-ley/Repository/sembako"
)

type repository struct {
	Public  public.PublicInterface
	Company company.CompanyInterface
	Sembako sembako.SembakoInterface
}

var AllRepository = repository{
	Company: company.InitCompany(),
	Sembako: sembako.InitSembako(),
	Public:  public.InitPublic(),
}
