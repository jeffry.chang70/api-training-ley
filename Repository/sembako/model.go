package sembako

import "api-training-ley/Controller/Dto"

type SembakoInterface interface {
	CreateSembako(params Dto.ReqCreateSembako) (id int, err error)
	ViewAllSembako() (resp []Dto.ResponseSembako, err error)
}

type sembako struct{}

func InitSembako() SembakoInterface {
	return &sembako{}
}
