package sembako

import (
	"api-training-ley/Connection"
	"api-training-ley/Controller/Dto"
)

//logic2 db

func (s sembako) CreateSembako(params Dto.ReqCreateSembako) (id int, err error) {
	connection, err := Connection.PostgresConn()
	if err != nil {
		return
	}
	query := `INSERT INTO public.sembako(sembako_name, sembako_qty, sembako_price, sembako_category)
				VALUES($1, $2, $3, $4) RETURNING sembako_id`
	//connection.Query() //esekusi return multiple value
	//connection.Exec() //esekusi tanpa return value
	err = connection.QueryRow(query, params.Name, params.Qty, params.Price, params.Category).Scan(&id) //esekusi return single row value
	return
}

func (s sembako) ViewAllSembako() (resp []Dto.ResponseSembako, err error) {
	connection, err := Connection.PostgresConn()
	if err != nil {
		return
	}
	query := `SELECT sembako_id, sembako_name, sembako_category, sembako_qty, sembako_price, TO_CHAR(created_at,'YYYY-MM-DD HH24:MI:SS') 
  				FROM public.sembako`
	rows, err := connection.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		var dataSembako Dto.ResponseSembako
		err = rows.Scan(&dataSembako.Id, &dataSembako.Name, &dataSembako.Category, &dataSembako.Qty,
			&dataSembako.Price, &dataSembako.CreatedAt)
		if err != nil {
			return
		}
		resp = append(resp, dataSembako)
	}
	return
}
