package company

type CompanyInterface interface {
	CreateCompany()
}

type company struct{}

func InitCompany() CompanyInterface {
	return &company{}
}
