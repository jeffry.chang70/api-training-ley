package public

import (
	"context"
	"time"
)

type PublicInterface interface {
	SetCache(ctx context.Context, keys string, data interface{}, expired time.Duration) (err error)
	GetCache(ctx context.Context, keys string, data interface{}) (err error)
	SetHMCache(ctx context.Context, keys string, data map[string]interface{}, expired time.Duration) (err error)
	GetHMCache(ctx context.Context, keys, field string, data interface{}) (err error)
	HGetAllCache(ctx context.Context, keys string) (res map[string]string, err error)
	HDelCache(ctx context.Context, keys string, field ...string) (err error)
	DelCache(ctx context.Context, keys ...string) (err error)
}

type public struct{}

func InitPublic() PublicInterface {
	return &public{}
}
