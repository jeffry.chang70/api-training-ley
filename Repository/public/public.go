package public

import (
	"api-training-ley/Connection"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

//set
func (p public) SetCache(ctx context.Context, keys string, data interface{}, expired time.Duration) (err error) {
	conn := Connection.RedisConn()
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return
	}
	err = conn.Set(ctx, keys, string(dataBytes), expired).Err()
	return
}

//get
func (p public) GetCache(ctx context.Context, keys string, data interface{}) (err error) {
	conn := Connection.RedisConn()
	dataCache, err := conn.Get(ctx, keys).Result()
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(dataCache), &data)
	return
}

//hmset
func (p public) SetHMCache(ctx context.Context, keys string, data map[string]interface{}, expired time.Duration) (err error) {
	conn := Connection.RedisConn()
	tempDataMap := make(map[string]string)
	for keys, val := range data {
		dataBytes, _ := json.Marshal(val)
		tempDataMap[keys] = string(dataBytes)
	}
	err = conn.HMSet(ctx, keys, tempDataMap).Err()
	if err != nil {
		return
	}
	err = conn.Expire(ctx, keys, expired).Err()
	return
}

//hmget
func (p public) GetHMCache(ctx context.Context, keys, field string, data interface{}) (err error) {
	conn := Connection.RedisConn()
	dataRedis, err := conn.HMGet(ctx, keys, field).Result()
	if err != nil {
		return
	}
	fmt.Println("DATA : ", dataRedis)
	if len(dataRedis) == 0 || dataRedis[0] == nil {
		err = errors.New("Data redis empty")
		return
	}
	err = json.Unmarshal([]byte(dataRedis[0].(string)), &data)
	return
}

//hgetall
func (p public) HGetAllCache(ctx context.Context, keys string) (res map[string]string, err error) {
	conn := Connection.RedisConn()
	res, err = conn.HGetAll(ctx, keys).Result()
	return
}

//hdel
func (p public) HDelCache(ctx context.Context, keys string, field ...string) (err error) {
	conn := Connection.RedisConn()
	err = conn.HDel(ctx, keys, field...).Err()
	return
}

//del
func (p public) DelCache(ctx context.Context, keys ...string) (err error) {
	conn := Connection.RedisConn()
	err = conn.Del(ctx, keys...).Err()
	return
}
